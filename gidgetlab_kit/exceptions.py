from gidgetlab import GitLabException


class PipelineFailure(GitLabException):
    """An exception representing a pipeline failure."""
